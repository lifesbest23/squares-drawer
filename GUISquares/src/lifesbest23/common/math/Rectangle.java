package lifesbest23.common.math;

import java.awt.Point;

public class Rectangle {
	private int maxX, maxY, minX, minY;
	private Point[] points;
	
	private int height, width;
	
	public Rectangle(Point p1, Point p2){
		maxX = Math.max(p1.x, p2.x);
		minX = Math.min(p1.x, p2.x);
		maxY = Math.max(p1.y, p2.y);
		minY = Math.min(p1.y, p2.y);
		
		height = maxY - minY +1;
		width = maxX - minX + 1;
		if(height != 1){
			points = new Point[2*(height - 1) + 2*(width - 1)];
			int counter = 0;
			for(int i = minX; i <= maxX; i++){
				points[counter++] = new Point(i, maxY);
				points[counter++] = new Point(i, minY);
			}
			for(int i = minY + 1; i <  maxY; i++){
				points[counter++] = new Point(maxX, i);
				points[counter++] = new Point(minX, i);
			}
		}
		else{
			points = new Point[width];
			for(int i = 0; i < points.length; i++)
				points[i] = new Point(i + minX, minY);
		}
	}
	
	public Rectangle(int x, int y, int height, int width){
		this(new Point(x, y), new Point(x + width, y + height)); 
	}
	
	public Point[] getPoints(){
		return points;
	}
	
	public int getMaxX(){
		return maxX;
	}
	public int getMinX(){
		return minX;
	}
	public int getMaxY(){
		return maxY;
	}
	public int getMinY(){
		return minY;
	}
	public int getHeight(){
		return height;
	}
	public int getWidth(){
		return width;
	}
	
}
