package lifesbest23.common.math;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Line {
	private Point[] points;
	private List<Point> pointsList;
	
	public Line(Point p1, Point p2){
		pointsList = new ArrayList<Point>();
		line(p1.x, p1.y, p2.x, p2.y);
	}
	
	private void line(int x,int y,int x2, int y2) {
	    int w = x2 - x ;
	    int h = y2 - y ;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0 ;
	    if (w<0) dx1 = -1 ; else if (w>0) dx1 = 1 ;
	    if (h<0) dy1 = -1 ; else if (h>0) dy1 = 1 ;
	    if (w<0) dx2 = -1 ; else if (w>0) dx2 = 1 ;
	    int longest = Math.abs(w) ;
	    int shortest = Math.abs(h) ;
	    if (!(longest>shortest)) {
	        longest = Math.abs(h) ;
	        shortest = Math.abs(w) ;
	        if (h<0) dy2 = -1 ; else if (h>0) dy2 = 1 ;
	        dx2 = 0 ;            
	    }
	    int numerator = longest >> 1 ;
	    for (int i=0;i<=longest;i++) {
	        putpixel(x,y) ;
	        numerator += shortest ;
	        if (!(numerator<longest)) {
	            numerator -= longest ;
	            x += dx1 ;
	            y += dy1 ;
	        } else {
	            x += dx2 ;
	            y += dy2 ;
	        }
	    }
	    points = pointsList.toArray( new Point[pointsList.size()]);
	}
	
	private void putpixel(int x, int y){
		pointsList.add(new Point(x,y));
	}
		
	public Point[] getPoints(){
		return points;
	}
}