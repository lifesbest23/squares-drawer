package lifesbest23.common.math;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Circle {
	public List<Point> points;
	
	public Circle(int xc, int yc, int r){
		
		points = new ArrayList<Point>();
		//version3(xc, yc, r, r);
		version4(xc, yc, r);
	}
	
	@SuppressWarnings("unused")
	private void version2 (int x0, int y0, int radius)
	{	//does work. Draws a perfect circle
	    int x = 0;
	    int y = radius;
	    int delta = 2 - 2 * radius;
	    int error = 0;

	    while(y >= 0)
	    {
	    	addPoints (x0, y0, x, y);
	        error = 2 * (delta + y) - 1;
	        if(delta < 0 && error <= 0) {
	            ++x;
	            delta += 2 * x + 1;
	            continue;
	        }
	        error = 2 * (delta - x) - 1;
	        if(delta > 0 && error > 0) {
	            --y;
	            delta += 1 - 2 * y;
	            continue;
	        }
	        ++x;
	        delta += 2 * (x - y);
	        --y;
	    }
	}
	

	@SuppressWarnings("unused")
	private void version3 (int x0, int y0, int width, int height)
	{	//Drawing an Ellipse
		int a2 = width * width;
		int b2 = height * height;
		int fa2 = 4 * a2, fb2 = 4 * b2;
		int x, y, sigma;
		
		/* first half */
		for (x = 0, y = height, sigma = 2*b2+a2*(1-2*height); b2*x <= a2*y; x++)
		{
			addPoints (x0, y0, x, y);
			if (sigma >= 0)
			{
				sigma += fa2 * (1 - y);
				y--;
			}
			sigma += b2 * ((4 * x) + 6);
		}
		
		/* second half */
		for (x = width, y = 0, sigma = 2*a2+b2*(1-2*width); a2*y <= b2*x; y++)
		{
			addPoints (x0, y0, x, y);
			if (sigma >= 0)
			{
				sigma += fb2 * (1 - x);
				x--;
			}
			sigma += a2 * ((4 * y) + 6);
		}
	}
	
	private void version4(int x0, int y0, int radius)
	{	//it draws a circle
	  int x = radius, y = 0;
	  int radiusError = 1-x;
	 
	  while(x >= y)
	  {
		  points.add(new Point(x + x0, y + y0));
		  points.add(new Point(y + x0, x + y0));
		  points.add(new Point(-x + x0, y + y0));
		  points.add(new Point(-y + x0, x + y0));
		  points.add(new Point(-x + x0, -y + y0));
		  points.add(new Point(-y + x0, -x + y0));
		  points.add(new Point(x + x0, -y + y0));
		  points.add(new Point(y + x0, -x + y0));
	 
	    y++;
		if(radiusError<0)
			radiusError+=2*y+1;
		else
	        {
			x--;
			radiusError+=2*(y-x+1);
		}
	  }
	}
	
	
	
	private void addPoints(int xc, int yc, int x, int y){
		points.add(new Point(xc + x, yc + y) );
		points.add(new Point(xc - x, yc + y) );
		points.add(new Point(xc + x, yc - y) );
		points.add(new Point(xc - x, yc - y) );
	}
	
	public Point[] getPoints(){
		Point[] ret = new Point[points.size()];
		for(int i = 0; i < points.size(); i++)
			ret[i] = points.get(i);
		return ret;
	}
}
