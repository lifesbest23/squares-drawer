package lifesbest23.common.swing.customDialogs;

import java.awt.Color;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class aboutDialog extends JDialog {
	private static final long serialVersionUID = 4722888848734452720L;
	
	public aboutDialog(JFrame frame, boolean modal){
		this(frame, modal, "This is an Application", 
				"No further Information provided, please Contact me for questions");
	}
	
	public aboutDialog(JFrame frame, boolean modal, String name, String message){
		super(frame, modal);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		JTextArea text = new JTextArea();
		text.setEditable(false);
		text.setForeground(Color.BLACK);
		text.setLineWrap(true);
		text.append(name + "\n\n");
		text.append("created by lifesbest23\n email goofy23.2@gmail.com\n\n");
		text.append(message);
		
		this.add(text);
		this.pack();
		this.setSize(400, 300);
		this.setVisible(true);
	}
}
