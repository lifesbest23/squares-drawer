package lifesbest23.common.swing;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;



public class WindowAdapter implements WindowListener{

	  @Override public void windowClosing( WindowEvent event ) { /*Empty*/ }
	  @Override public void windowClosed( WindowEvent event ) { /*Empty*/ }
	  @Override public void windowDeiconified( WindowEvent event ) { /*Empty*/ }
	  @Override public void windowIconified( WindowEvent event ) { /*Empty*/ }
	  @Override public void windowActivated( WindowEvent event ) { /*Empty*/ }
	  @Override public void windowDeactivated( WindowEvent event ) { /*Empty*/ }
	  @Override public void windowOpened( WindowEvent event ) { /*Empty*/ }

}
