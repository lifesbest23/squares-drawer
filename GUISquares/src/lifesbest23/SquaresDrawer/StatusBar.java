package lifesbest23.SquaresDrawer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusBar extends JPanel{
	private static final long serialVersionUID = 3495107899348964145L;

	
	private JLabel coordsColor, toolInfo;
	
	public StatusBar(){
		super();
		
		this.setLayout(new BorderLayout());
		
		coordsColor = new JLabel(String.format("  %d / %d    -    color", 0, 0), JLabel.LEFT);
		toolInfo = new JLabel("toolInfo  ");
		
		this.add(coordsColor, BorderLayout.WEST);
		this.add(toolInfo, BorderLayout.EAST);
		
		this.setMaximumSize(new Dimension(2000, 25));
		
		//this.setSize();
	}
	
	public void setCoords(int x, int y, Color color){
		if(x == -1 || y == -1)
			coordsColor.setText("  -- / --    -    XXXXX");
		else
			coordsColor.setText(String.format("  %d / %d    -    %05X", x, y, color.getRGB()));
	}
	
	public void setToolInfo(String toolName, String toolText){
		toolInfo.setText(String.format("%s : %s   ", toolName, toolText));
	}

}
