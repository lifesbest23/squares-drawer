package lifesbest23.SquaresDrawer;

import java.util.ArrayList;
import java.util.List;

public class History {
	private List<HistoryEntry> entrys;
	private int max = 0;
	private int state = 0;

	public History(HistoryEntry e){
		entrys = new ArrayList<HistoryEntry>();
		entrys.add(e);
	}
	
	public void addState(HistoryEntry e){
		entrys.add(++state, e);
		max = state;
		System.out.println(e);
	}
	
	public HistoryEntry undo(){
		if(state > 0)
			return entrys.get(--state);
		return null;
	}
	
	public HistoryEntry redo(){
		if(state < entrys.size() - 1)
			return entrys.get(++state);
		return null;
	}
	
	public HistoryEntry getActive(){
		return entrys.get(state);
	}
	
	public boolean isStart(){
		return state == 0;
	} 
	
	public boolean isEnd(){
		return state == max;
	}
}
