package lifesbest23.SquaresDrawer;

import java.awt.Color;
import java.util.Arrays;
import java.util.Date;

public class HistoryEntry {
	private String name;
	private Color[][] colors;
	private Date timeStamp;
	
	public HistoryEntry(String name, Color[][] colors){
		this.name = name;
		this.colors = deepCopy(colors); //Arrays.copyOf(colors, colors.length);
		timeStamp = new Date();
	}
	
	public String toString(){
		return String.format("%s ( %tT ) - %s", name, timeStamp, Arrays.deepToString(colors));
	}
	
	public Color[][] getColors(){
		return deepCopy(colors);
	}
	
	public String getName(){
		return name;
	}
	
	public Date getTimeStamp(){
		return timeStamp;
	}
	
	public static Color[][] deepCopy(Color[][] original) {
	    if (original == null) {
	        return null;
	    }

	    final Color[][] result = new Color[original.length][];
	    for (int i = 0; i < original.length; i++) {
	        result[i] = Arrays.copyOf(original[i], original[i].length);
	        // For Java versions prior to Java 6 use the next:
	        // System.arraycopy(original[i], 0, result[i], 0, original[i].length);
	    }
	    return result;
	}
}
