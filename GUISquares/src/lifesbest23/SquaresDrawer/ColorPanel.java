package lifesbest23.SquaresDrawer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;


public class ColorPanel extends JPanel implements MouseListener{
	private static final long serialVersionUID = -7788441658992620551L;
	
	private SquaresPanel sp;
	private Color[] colors = {Color.BLACK, Color.WHITE, Color.BLUE, Color.CYAN, Color.DARK_GRAY,
		Color.GRAY, Color.LIGHT_GRAY, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED,
		Color.YELLOW};
	
	private int clickCountTimer = 0;
	private int button, cx, cy;
	private Timer t;
	
	public ColorPanel(SquaresPanel sp){
		super();
		this.sp = sp;
		this.setPreferredSize(new Dimension(90,300));
		this.setMaximumSize(new Dimension(90,300));
		this.setMinimumSize(new Dimension(90,300));
		this.addMouseListener(this);
	}
	
	@SuppressWarnings("unused")
	private void setRandomColors(){
		Random r = new Random();
		for(int i = 0; i < colors.length; i++)
			colors[i] = new Color(r.nextInt(256), r.nextInt(256), r.nextInt(256));
	}
	
	@Override public void paintComponent(Graphics g){
		g.drawString("L", 8, 15);
		g.drawString("M", 38, 15);
		g.drawString("R", 68, 15);
		Color[] currentColors = sp.getClickColorArray();
		int cc = 0;
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++){
				g.setColor(currentColors[cc++]);
				g.fillRect( i*30 + 5, j*30 + 20, 20, 20);
				g.setColor(Color.BLACK);
				g.drawRect( i*30 + 5, j*30 + 20, 20, 20);
			}
		g.drawLine(0, 110, 90, 110);
		int x = 5;
		int y = 120;
		for(Color c:this.colors){
			if(x > 85){
				x = 5;
				y += 30;
			}
			g.setColor(c);
			g.fillRect(x, y, 20, 20);
			g.setColor(Color.BLACK);
			g.drawRect(x, y, 20, 20);
			x += 30;
		}
		
	}
	
	@Override public void mouseClicked(MouseEvent e) {
		if(clickCountTimer < 3)
			clickCountTimer++;
		
		int tempbutton = e.getButton();
		switch(tempbutton){
		case 1:
			tempbutton = 0;
			break;
		case 2:
			tempbutton = 3;
			break;
		case 3:
			tempbutton = 6;
			break;
		default:
			return;
		}
		if(cx != e.getX() || cy != e.getY() ||
				button != tempbutton){
			button = tempbutton;
			cx = e.getX();
			cy = e.getY();
			
			if(t != null)
				t.stop();
			clickCountTimer = 1;
			
			t = new Timer(400, new ActionListener(){
				@Override public void actionPerformed(ActionEvent e) {
					((Timer)e.getSource()).stop();
					//System.out.println(cx + " " + cy + " " + button);
					int x = 5;
					int y = 120;
					if(cy < 120)
						return;
					for(Color c:colors){
						if(x > 85){
							x = 5;
							y += 30;
						}
						//System.out.println(x + " " + y);
						if(cx > x && cx < x + 20 && cy > y && cy < y + 20){
							sp.setClickColor(button , clickCountTimer - 1, c);
							repaint();
							clickCountTimer = 0;
							return;
						}
						x += 30;
					}
				}
			});
			t.start();
		}
	}

	@Override public void mouseEntered(MouseEvent e) {}

	@Override public void mouseExited(MouseEvent e) {}

	@Override public void mousePressed(MouseEvent e) {}

	@Override public void mouseReleased(MouseEvent e) {
		
	}
}
