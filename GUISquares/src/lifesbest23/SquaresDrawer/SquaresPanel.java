package lifesbest23.SquaresDrawer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.Scrollable;

import lifesbest23.common.math.Circle;
import lifesbest23.common.math.Line;


public class SquaresPanel extends JPanel implements Scrollable{
	private static final long serialVersionUID = 1424064721337488716L;
	
	private int squareSize = 45;
	private Color[][] colors;
	private Color[][] toolColors;
	private Point selected;
	private History history;
	private Color[] clickColors = {Color.RED, Color.BLUE, Color.GREEN,
			Color.WHITE, Color.WHITE, Color.WHITE,
			Color.WHITE, Color.BLACK, Color.GRAY};
	
	//Actually Senseless, because not used
	public final int LEFT = 0;
	public final int MIDDLE = 3;
	public final int RIGHT = 6;
	public final int ONCE_CLICKED = 0;
	public final int TWICE_CLICKED = 1;
	public final int TRICE_CLICKED = 2;
	
	private boolean grid = true;
	private boolean scale = false;
	
	private String toolName, toolText;
	
	private SquaresFrame sf;
	private StatusBar sb;
	
	private int x, y;
	
	public SquaresPanel(int x, int y, SquaresFrame squaresFrame, StatusBar sb){
		super();
		
		//NormalColors
		colors = new Color[x][y];
		for(Color[] l:colors)
			for(int i = 0; i < y; i++)
				l[i] = Color.WHITE;
		
		//ToolColors
		toolColors = new Color[x][y];
		Color clearColor = new Color(0,0,0,0);
		for(Color[] l:toolColors)
			for(int i = 0; i < y; i++)
				l[i] = clearColor;
		
		history = new History(new HistoryEntry("Blank", colors));
		
		this.x = x;
		this.y = y;
		selected = new Point(0, 0);
		sf = squaresFrame;
		this.sb = sb;
		
		setPreferredSize(new Dimension(squareSize * x, squareSize *y));
		
		this.setFreeDrawTool();
		StatusUpdateListener sul = new StatusUpdateListener();
		this.addMouseListener(sul);
		this.addMouseMotionListener(sul);
	}

	@Override protected void paintComponent( Graphics g )
	{
		super.paintComponent( g );
	    for ( int y = 0; y < (squareSize*this.y); y += squareSize )
	    	for ( int x = 0; x < (squareSize*this.x); x += squareSize )
	    	{
	    		if(toolColors[x/squareSize][y/squareSize].getAlpha() == 0){
	    			g.setColor( history.getActive().getColors()[x/squareSize][y/squareSize] );
	    			g.fillRect( x, y, squareSize, squareSize );
	    		}
	    		else{
	    			g.setColor( toolColors[x/squareSize][y/squareSize] );
	    			g.fillRect( x, y, squareSize, squareSize );
	    		}
	    		if(selected.equals(new Point(x/squareSize,y/squareSize)))
	    		{
	    			g.setColor( Color.YELLOW);
	    			g.drawRect( x, y, squareSize, squareSize );
	    			g.drawRect( x+1, y+1, squareSize-2, squareSize-2 );
	    		}
	    		else if(grid){
	    			g.setColor( Color.BLACK );
	    			g.drawRect( x, y, squareSize, squareSize );
	    		}
	    	}
	}
	
	//private functions
	private void drawToolColor(){
		for(int i = 0; i < toolColors.length; i++)
			for(int j = 0; j < toolColors[0].length; j++)
				if(toolColors[i][j].getAlpha() != 0){
					colors[i][j] = toolColors[i][j];
					toolColors[i][j] = new Color(0,0,0,0);
				}
		repaint();
		sf.clearSaved();
	}
	
	private void setHistoryState(String name){
		history.addState(new HistoryEntry(name, colors));
	}
	
	//Old
	public void selectionLeft() {
		if(selected.x >0)
			selected.x -= 1;
		else
			System.out.println("Already Left");
		repaint();
	}
	//Old
	public void selectionRight() {
		if(selected.x < x -1)
			selected.x += 1;
		else
			System.out.println("Already Right");
		repaint();
	}
	//Old
	public void selectionDown() {
		if(selected.y < y -1)
			selected.y += 1;
		else
			System.out.println("Already Down");
		repaint();
	}
	//Old
	public void selectionUp() {
		if(selected.y > 0)
			selected.y -= 1;
		else
			System.out.println("Already Up");
		repaint();
	}
	//Old
	public void selectionSet(){
		colors[selected.x][selected.y] = Color.CYAN;
		repaint();
		sf.clearSaved();
	}
	
	//Public functions
	public void zoomIn(){
		if(squareSize < 100)
			squareSize += 1;
		setPreferredSize(new Dimension(squareSize*colors.length, squareSize*colors[0].length));
		repaint();
	}
	
	public void zoomOut(){
		if(squareSize > 1)
			squareSize -= 1;
		setPreferredSize(new Dimension(squareSize*colors.length, squareSize*colors[0].length));
		repaint();
	}
	
	public void setGrid(boolean g){
		grid = g;
		repaint();
	}
	
	public boolean getGrid(){
		return grid;
	}
	
	public void setScale(boolean s){
		scale = s;
		repaint();
	}
	
	public boolean getScale(){
		return scale;
	}
	
	public void undo(){
		if(!history.isStart())
			colors = history.undo().getColors();
		repaint();
	}
	
	public void redo(){
		if(!history.isEnd())
			colors = history.redo().getColors();
		repaint();
	}
	
	public Color getClickColor(int button, int clickcount){
		if(0 <= button + clickcount && button + clickcount <= 8)
			return clickColors[button + clickcount];
		return null;
	}
	
	public Color[] getClickColorArray(){
		return clickColors;
	}
	public void setClickColor(int button, int clickcount, Color color){
		if(0 <= button + clickcount && button + clickcount <= 8)
			clickColors[button + clickcount] = color;
	}
	
	public void load(Color[][] c, Point p, int x, int y){
		colors = c;
		selected = p;
		this.x = x;
		this.y = y;
		toolColors = new Color[x][y];
		
		Color clearColor = new Color(0,0,0,0);
		for(Color[] l:toolColors)
			for(int i = 0; i < y; i++)
				l[i] = clearColor;
		
		history = new History(new HistoryEntry("Opened File", colors));
		
		setPreferredSize(new Dimension(squareSize*colors.length, squareSize*colors[0].length));
		repaint();
	}
	
	public Color[][] exportColors(){
		return colors;
	}
	
	public int exportX(){
		return x;
	}
	
	public int exportY(){
		return y;
	}
	
	public Point exportSelected(){
		return selected;
	}
	
	public void plane(int x, int y){
		colors = new Color[x][y];
		for(Color[] l:colors)
			for(int i = 0; i < y; i++)
				l[i] = Color.WHITE;
		
		toolColors = new Color[x][y];
		Color clearColor = new Color(0,0,0,0);
		for(Color[] l:toolColors)
			for(int i = 0; i < y; i++)
				l[i] = clearColor;
		
		this.x = x;
		this.y = y;
		
		selected = new Point(0, 0);
		
		history = new History(new HistoryEntry("New File", colors));

		
		setPreferredSize(new Dimension(squareSize*colors.length, squareSize*colors[0].length));
		repaint();
	}
	
	public void setRectangleTool(){
		for(MouseMotionListener l:this.getMouseMotionListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseMotionListener(l);
		for(MouseListener l:this.getMouseListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseListener(l);
		
		toolName = "Draw Rectangle";
		toolText = " - ";
		
		sb.setToolInfo(toolName, toolText);
		
		DrawRectangelTool l = new DrawRectangelTool();
		this.addMouseListener(l);
		this.addMouseMotionListener(l);
	}
	
	public void setFreeDrawTool(){
		for(MouseMotionListener l:this.getMouseMotionListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseMotionListener(l);
		for(MouseListener l:this.getMouseListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseListener(l);
		
		toolName = "Draw Free";
		toolText = " - ";
		
		sb.setToolInfo(toolName, toolText);
		
		FreeDrawTool l = new FreeDrawTool();
		this.addMouseListener(l);
		this.addMouseMotionListener(l);
	}
	
	public void setCircleTool(){
		for(MouseMotionListener l:this.getMouseMotionListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseMotionListener(l);
		for(MouseListener l:this.getMouseListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseListener(l);
		
		toolName = "Draw Circle";
		toolText = " - ";
		
		sb.setToolInfo(toolName, toolText);
		
		DrawCircleTool l = new DrawCircleTool();
		this.addMouseListener(l);
		this.addMouseMotionListener(l);
	}
	
	public void setLineTool(){
		for(MouseMotionListener l:this.getMouseMotionListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseMotionListener(l);
		for(MouseListener l:this.getMouseListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseListener(l);
		
		toolName = "Draw Line";
		toolText = " - ";
		
		sb.setToolInfo(toolName, toolText);
		
		DrawLineTool l = new DrawLineTool();
		this.addMouseListener(l);
		this.addMouseMotionListener(l);
	}
	
	public void setFillAreaTool(){
		for(MouseMotionListener l:this.getMouseMotionListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseMotionListener(l);
		for(MouseListener l:this.getMouseListeners())
			if(!(l instanceof StatusUpdateListener))
				this.removeMouseListener(l);
		
		toolName = "Fill Area";
		toolText = " - ";
		
		sb.setToolInfo(toolName, toolText);
		
		FillAreaTool l = new FillAreaTool();
		this.addMouseListener(l);
		this.addMouseMotionListener(l);
	}
	
	@Override public Dimension getPreferredScrollableViewportSize() {
		return new Dimension(squareSize*10, squareSize*10);
	}

	@Override public int getScrollableBlockIncrement(java.awt.Rectangle arg0, int arg1, int arg2) {
		return squareSize * 9;
	}

	@Override public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	@Override public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	@Override public int getScrollableUnitIncrement(java.awt.Rectangle arg0, int arg1, int arg2) {
		return squareSize;
	}
	
	public class StatusUpdateListener implements MouseMotionListener, MouseListener{
		
		private int oldX = -1;
		private int oldY = -1;
		
		@Override public void mouseDragged(MouseEvent e) {}
		
		@Override public void mouseClicked(MouseEvent e) {}

		@Override public void mouseEntered(MouseEvent e) {
			int x = e.getX()/squareSize;
			int y = e.getY()/squareSize;
			sb.setCoords(x, y, colors[x][y]);
			
			sb.setToolInfo(toolName, toolText);
			
			oldX = x;
			oldY = y;
		}

		@Override public void mouseExited(MouseEvent e) {
			sb.setCoords(-1, -1, null);
			
			sb.setToolInfo(toolName, toolText);
			
			oldX = -1;
			oldY = -1;
		}

		@Override public void mousePressed(MouseEvent e) {}
		
		@Override public void mouseReleased(MouseEvent e) {}

		@Override public void mouseMoved(MouseEvent e) {
			if(x != oldX || y != oldY){
				int x = e.getX()/squareSize;
				int y = e.getY()/squareSize;
				sb.setCoords(x, y, colors[x][y]);
				
				sb.setToolInfo(toolName, toolText);
				
				oldX = x;
				oldY = y;
			}
		}
		
	}
	
	public class FreeDrawTool implements MouseMotionListener, MouseListener{
		private int buttonPressed = -1;
		private int clickCount = 0;
		private int xtOld = -1;
		private int ytOld = -1;
		
		@Override public void mouseClicked(MouseEvent e) {}

		@Override public void mouseEntered(MouseEvent e) {}

		@Override public void mouseExited(MouseEvent e) {}

		@Override public void mousePressed(MouseEvent e) {
			switch(e.getButton()){
			case 1:
				buttonPressed = 0;
				break;
			case 2:
				buttonPressed = 3;
				break;
			case 3:
				buttonPressed = 6;
				break;
			default:
				return;
			}
			clickCount = e.getClickCount();

			xtOld = e.getX()/squareSize;
			ytOld = e.getY()/squareSize;
		}
		
		@Override public void mouseReleased(MouseEvent e) {
			int xt = e.getX();
			int yt = e.getY();
			if(xt > squareSize*x || yt > squareSize*y)
				return;
			
			if(e.getClickCount() > 0 && e.getClickCount() < 4)
				toolColors[xt/squareSize][yt/squareSize] = clickColors[buttonPressed + clickCount - 1];
			
			drawToolColor();
			
			setHistoryState("FreeDrawTool");
			sf.clearSaved();
			
			//resetting Variables
			buttonPressed = -1;
			clickCount = 0;
			xtOld = -1;
			ytOld = -1;
		}
		
		@Override public void mouseDragged(MouseEvent e) {
			new FreeDrawToolThread(e).start();
		}

		@Override public void mouseMoved(MouseEvent e) {}
		
		class FreeDrawToolThread extends Thread
		{
			MouseEvent e;
			FreeDrawToolThread( MouseEvent e ){
				super();
				this.e = e;
			}
			public void run() {
				if(buttonPressed > -1){
					int xt = e.getX()/squareSize;
					int yt = e.getY()/squareSize;
					
					if(xt >= squareSize*x || yt >= squareSize*y || xt < 0 || yt < 0)
						stopThread();
					
					Line l = new Line(new Point(xt, yt),
							new Point(xtOld, ytOld));
					Point[] ps = l.getPoints();
					for(Point p:ps)
						if(p.x >= 0 && p.x < x && p.y >= 0 && p.y < y)
							toolColors[p.x][p.y] = clickColors[buttonPressed + clickCount - 1];
					
					repaint();
					sf.clearSaved();

					xtOld = xt;
					ytOld = yt;
				}
				stopThread();
			}
			
			@SuppressWarnings("deprecation")
			private void stopThread(){
				this.stop();
			}
		}
	}
	
	public class DrawRectangelTool implements MouseMotionListener, MouseListener {
		private int buttonPressed = -1;
		private int clickCount = 0;
		private int startX = 0;
		private int startY = 0;
		private int xtOld = -1;
		private int ytOld = -1;
				
		@Override public void mouseClicked(MouseEvent e) {}

		@Override public void mouseEntered(MouseEvent e) {}

		@Override public void mouseExited(MouseEvent e) {}

		@Override public void mousePressed(MouseEvent e) {
			switch(e.getButton()){
			case 1:
				buttonPressed = 0;
				break;
			case 2:
				buttonPressed = 3;
				break;
			case 3:
				buttonPressed = 6;
				break;
			default:
				return;
			}
			clickCount = e.getClickCount();
			startX = e.getX()/squareSize;
			startY = e.getY()/squareSize;
		}
		
		@Override public void mouseReleased(MouseEvent e) {
			int xt = e.getX()/squareSize;
			int yt = e.getY()/squareSize;
			
			if(xt >= x || yt >= y)
				return;
			
			if(e.getClickCount() > 0 && e.getClickCount() < 4)
				colors[xt][yt] = clickColors[buttonPressed + clickCount - 1];
			
			drawToolColor();
			sf.clearSaved();
			setHistoryState("DrawRectangleTool");
			//ResetValues
			buttonPressed = -1;
			clickCount = 0;
			xtOld = -1;
			ytOld = -1;
			//resest ToolText
			toolText = " - ";
		}
		
		@Override public void mouseDragged(MouseEvent e) {
			new DrawRectangelToolThread(e).start();
		}

		@Override public void mouseMoved(MouseEvent e) {}
		
		class DrawRectangelToolThread extends Thread
		{
			MouseEvent e;
			DrawRectangelToolThread( MouseEvent e ){
				super();
				this.e = e;
			}
			public void run() {
				if(buttonPressed > -1){
					int xt = e.getX()/squareSize;
					int yt = e.getY()/squareSize;
					
					//Check if it is out of area
					if(xt >= x || yt >= y || xt < 0 || yt < 0)
						stopThread();
					
					if(xtOld != xt || ytOld != yt){
						//clear toolColors
						Color clearColor = new Color(0,0,0,0);
						for(Color[] l:toolColors)
							for(int i = 0; i < y; i++)
								l[i] = clearColor;
						
						//Create rectangle
						lifesbest23.common.math.Rectangle rect = 
								new lifesbest23.common.math.Rectangle(new Point(xt, yt),
										new Point(startX, startY));
						//And write the points to the ToolColors
						for(Point p:rect.getPoints())
							toolColors[p.x][p.y] = clickColors[buttonPressed + clickCount - 1];
						
						//repaint
						repaint();
						sf.clearSaved();
						xtOld = xt;
						ytOld = yt;
						//setToolText
						toolText = String.format("From: %d / %d To: %d / %d  Height: %d WIdth: %d",
								startX, startY, xt, xt, xt - startX + 1, yt - startY + 1);
						sb.setToolInfo(toolName, toolText);
					}
					
				}
				stopThread();
			}
			
			@SuppressWarnings("deprecation")
			private void stopThread(){
				this.stop();
			}
		}
	}
	
	public class DrawCircleTool implements MouseMotionListener, MouseListener {
		private int buttonPressed = -1;
		private int clickCount = 0;
		private int startX = 0;
		private int startY = 0;
		private int xtOld = -1;
		private int ytOld = -1;
				
		@Override public void mouseClicked(MouseEvent e) {}

		@Override public void mouseEntered(MouseEvent e) {}

		@Override public void mouseExited(MouseEvent e) {}

		@Override public void mousePressed(MouseEvent e) {
			switch(e.getButton()){
			case 1:
				buttonPressed = 0;
				break;
			case 2:
				buttonPressed = 3;
				break;
			case 3:
				buttonPressed = 6;
				break;
			default:
				return;
			}
			clickCount = e.getClickCount();
			startX = e.getX()/squareSize;
			startY = e.getY()/squareSize;
		}
		
		@Override public void mouseReleased(MouseEvent e) {
			int xt = e.getX();
			int yt = e.getY();
			if(xt > squareSize*x || yt > squareSize*y)
				return;
			
			drawToolColor();
			sf.clearSaved();
			setHistoryState("DrawCircleTool");
			//Reset Values
			buttonPressed = -1;
			clickCount = 0;
			xtOld = -1;
			ytOld = -1;
			//resest ToolText
			toolText = " - ";
		}
		
		@Override public void mouseDragged(MouseEvent e) {
			new DrawCircleToolThread(e).start();
		}

		@Override public void mouseMoved(MouseEvent e) {}
		
		class DrawCircleToolThread extends Thread
		{
			MouseEvent e;
			DrawCircleToolThread( MouseEvent e ){
				super();
				this.e = e;
			}
			public void run() {
				if(buttonPressed > -1){
					int xt = e.getX()/squareSize;
					int yt = e.getY()/squareSize;
					if(xt >= x || yt >= y || xt < 0 || yt < 0)
						stopThread();
					
					if(xtOld != xt || ytOld != yt){
						//clear toolColors
						Color clearColor = new Color(0,0,0,0);
						for(Color[] l:toolColors)
							for(int i = 0; i < y; i++)
								l[i] = clearColor;
						
						//create Circle
						Circle c = new Circle(startX, startY, 
								(int)Point.distance(startX, startY,
										xt, yt));
						//
						Point[] ps = c.getPoints();
						for(Point p:ps)
							if(p.x >= 0 && p.x < x && p.y >= 0 && p.y < y)
								toolColors[p.x][p.y] = clickColors[buttonPressed + clickCount - 1];
						
						repaint();
						xtOld = xt;
						ytOld = yt;
						toolText = String.format("From: %d / %d  Radius: %d",
								startX, startY, (int)Point.distance(startX, startY, xt, yt) + 1);
						sb.setToolInfo(toolName, toolText);
					}
					sf.clearSaved();
				}
				stopThread();
			}
			
			@SuppressWarnings("deprecation")
			private void stopThread(){
				this.stop();
			}
		}
	}
	
	public class DrawLineTool implements MouseMotionListener, MouseListener {
		private int buttonPressed = -1;
		private int clickCount = 0;
		private int startX = 0;
		private int startY = 0;
		private int xtOld = -1;
		private int ytOld = -1;
				
		@Override public void mouseClicked(MouseEvent e) {}

		@Override public void mouseEntered(MouseEvent e) {}

		@Override public void mouseExited(MouseEvent e) {}

		@Override public void mousePressed(MouseEvent e) {
			switch(e.getButton()){
			case 1:
				buttonPressed = 0;
				break;
			case 2:
				buttonPressed = 3;
				break;
			case 3:
				buttonPressed = 6;
				break;
			default:
				return;
			}
			clickCount = e.getClickCount();
			startX = e.getX()/squareSize;
			startY = e.getY()/squareSize;
		}
		
		@Override public void mouseReleased(MouseEvent e) {
			int xt = e.getX();
			int yt = e.getY();
			if(xt > squareSize*x || yt > squareSize*y)
				return;
			
			drawToolColor();
			sf.clearSaved();
			setHistoryState("DrawLineTool");
			
			buttonPressed = -1;
			clickCount = 0;
			xtOld = -1;
			ytOld = -1;
			//resest ToolText
			toolText = " - ";
		}
		
		@Override public void mouseDragged(MouseEvent e) {
			new DrawLineToolThread(e).start();
		}

		@Override public void mouseMoved(MouseEvent e) {}
		
		class DrawLineToolThread extends Thread
		{
			MouseEvent e;
			DrawLineToolThread( MouseEvent e ){
				super();
				this.e = e;
			}
			public void run() {
				if(buttonPressed > -1){
					int xt = e.getX()/squareSize;
					int yt = e.getY()/squareSize;
					if(xt >= x || yt >= y || xt < 0 || yt < 0)
						stopThread();
					//clear temp
					if(xtOld != xt || ytOld != yt){
						Color clearColor = new Color(0,0,0,0);
						for(Color[] l:toolColors)
							for(int i = 0; i < y; i++)
								l[i] = clearColor;;
						//create Line
						Line c = new Line(new Point(xt, yt),
									new Point(startX, startY));
						Point[] ps = c.getPoints();
						for(Point p:ps)
							if(p.x >= 0 && p.x < x && p.y >= 0 && p.y < y)
								toolColors[p.x][p.y] = clickColors[buttonPressed + clickCount - 1];
						
						repaint();
						xtOld = xt;
						ytOld = yt;
						//setToolText
						toolText = String.format("From: %d / %d To: %d / %d  Length: %d",
								startX, startY, xt, xt, (int)Point.distance(startX, startY, xt, yt) + 1);
						sb.setToolInfo(toolName, toolText);
					}
					sf.clearSaved();
				}
				stopThread();
			}
			
			@SuppressWarnings("deprecation")
			private void stopThread(){
				this.stop();
			}
		}
	}

	public class FillAreaTool implements MouseMotionListener, MouseListener{
		
		@Override public void mouseClicked(MouseEvent e) {}

		@Override public void mouseEntered(MouseEvent e) {}

		@Override public void mouseExited(MouseEvent e) {}

		@Override public void mousePressed(MouseEvent e) {}
		
		@Override public void mouseReleased(MouseEvent e) {
			new FillAreaToolThread(e).start();
		}
		
		@Override public void mouseDragged(MouseEvent e) {
			
		}

		@Override public void mouseMoved(MouseEvent e) {}
		
		class FillAreaToolThread extends Thread
		{
			MouseEvent e;
			int buttonPressed;
			int clickCount;
			
			FillAreaToolThread( MouseEvent e ){
				super();
				this.e = e;
			}
			public void run() {
				if(e.getClickCount() < 4){
					
					switch(e.getButton()){
					case 1:
						buttonPressed = 0;
						break;
					case 2:
						buttonPressed = 3;
						break;
					case 3:
						buttonPressed = 6;
						break;
					default:
						return;
					}
					clickCount = e.getClickCount();
					int xt = e.getX()/squareSize;
					int yt = e.getY()/squareSize;
					
					if(xt >= x || yt >= y || xt < 0 || yt < 0)
						stopThread();
					
					if(!colors[xt][yt].equals(clickColors[buttonPressed + clickCount - 1]))
						fill(xt, yt, colors[xt][yt]);
					
					//resetting Variables
					buttonPressed = -1;
					clickCount = 0;
					
					repaint();
					sf.clearSaved();
					setHistoryState("FillAreaTool");
				}
				stopThread();
			}
			
			private void fill(int xp, int yp, Color original){
				if(xp >= 0 && xp < x && yp >= 0 && yp < y)
					if(colors[xp][yp].equals(original)){
						colors[xp][yp] = clickColors[buttonPressed + clickCount - 1];
						fill( xp + 1, yp, original);
						fill( xp - 1, yp, original);
						fill( xp, yp + 1, original);
						fill( xp, yp - 1, original);
					}
			}
			
			@SuppressWarnings("deprecation")
			private void stopThread(){
				this.stop();
			}
		}
	}
}
