package lifesbest23.SquaresDrawer;
//lifesbest23 24.01.2014

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import lifesbest23.SquaresDrawer.ColorPanel;
import lifesbest23.SquaresDrawer.NewDialog;
import lifesbest23.SquaresDrawer.SquaresPanel;
import lifesbest23.SquaresDrawer.StatusBar;
import lifesbest23.common.swing.WindowAdapter; 
import lifesbest23.common.swing.customDialogs.aboutDialog; 


public class SquaresFrame extends JFrame implements KeyListener{
	private static final long serialVersionUID = 5450189371386324784L;
	
	private SquaresPanel s;
	private JScrollPane scrollPane1;
	private StatusBar statusBar;
	
	private String path = null;
	private boolean saved = false;
	
	private Action newAction, saveAction, saveAsAction, openAction, exitAction, aboutAction,
	undoAction, redoAction, zoomInAction, zoomOutAction, gridAction,
	penToolAction, circleToolAction, rectangleToolAction, lineToolAction, fillAreaToolAction;
	
	SquaresFrame(String path){
		super();
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setSize(400, 400);
		this.setTitle(String.format("Squares Editor - *New File - %d_%d*", 10, 10));
		this.addWindowListener(new WindowAdapter(){
			@Override public void windowClosing( WindowEvent event )
	    	{
				exit();
	    	}
		});
		this.setFocusable(true);
		
		Box innerBox = new Box(BoxLayout.X_AXIS);
		Box outerBox = new Box(BoxLayout.Y_AXIS);

		statusBar = new StatusBar();
		s = new SquaresPanel(10,10, this, statusBar);
		scrollPane1 = new JScrollPane(s);
		
		
		innerBox.add(scrollPane1);
		innerBox.add(new ColorPanel(s));
		
		outerBox.add(innerBox);
		outerBox.add(statusBar);
		this.add(outerBox);

		this.addKeyListener(this);
		
		initActions();
		initMenuBar();
		initToolBar();
		if(path.length() > 0)//not no path
			open(new File(path));
		
		this.pack();
		this.setVisible(true);
	}

	@SuppressWarnings("serial")
	private void initActions(){
		newAction = new AbstractAction() {
			{
				putValue( Action.NAME,  "New File..." );
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/document-new.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Create new File");
				
			}
			public void actionPerformed( ActionEvent e ) {
				newFile();
			}
		};
		saveAction = new AbstractAction() {
			{
				putValue( Action.NAME,  "SAVE" );
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/document-save.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Save File");
				
			}
			public void actionPerformed( ActionEvent e ) {
				save();
			}
		};
		saveAsAction = new AbstractAction() {
			{
				putValue( Action.NAME,  "Save As..." );
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/document-save-as.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, 
						InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Save File As");
				
			}
			public void actionPerformed( ActionEvent e ) {
				saveAs();
			}
		};
		openAction = new AbstractAction() {
			{
				putValue( Action.NAME,  "Open..." );
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/document-open.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Open File");
				
			}
			public void actionPerformed( ActionEvent e ) {
				open();
			}
		};
		exitAction = new AbstractAction( "Exit" ) {
			public void actionPerformed( ActionEvent e ) {
				exit();
			}
		};
		undoAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Undo");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
						"resoureces/icons/px16/edit-undo.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Undo");
			}
			public void actionPerformed( ActionEvent e ) {
				undo();
			}
		};
		redoAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Redo");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/edit-redo.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Redo");
			}
			public void actionPerformed( ActionEvent e ) {
				redo();
			}
		};
		zoomInAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Zoom In");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/zoom-plus.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Zoom In");
			}
			public void actionPerformed( ActionEvent e ) {
				zoomIn();
			}
		};
		zoomOutAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Zoom Out");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/zoom-minus.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Zoom Out");
			}
			public void actionPerformed( ActionEvent e ) {
				zoomOut();
			}
		};
		gridAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Grid");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/edit-grid.png")));
				putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Grid");
			}
			public void actionPerformed( ActionEvent e ) {
				toggleGrid();
			}
		};
		penToolAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Pen");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/draw-pen.png")));
				//putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Free Drawing Pen");
			}
			public void actionPerformed( ActionEvent e ) {
				toolChangePen();
			}
		};
		circleToolAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Circle");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/form-circle.png")));
				//putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Draw a Circle");
			}
			public void actionPerformed( ActionEvent e ) {
				toolChangeCircle();
			}
		};
		rectangleToolAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Rectangle");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/form-rectangle.png")));
				//putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Draw a Rectangle");
			}
			public void actionPerformed( ActionEvent e ) {
				toolChangeRectangle();
			}
		};
		lineToolAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Line");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/form-line.png")));
				//putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Draw a Line");
			}
			public void actionPerformed( ActionEvent e ) {
				toolChangeLine();
			}
		};
		fillAreaToolAction = new AbstractAction(){
			{
				putValue( Action.NAME, "Fill");
				putValue( Action.SMALL_ICON, new ImageIcon(
						getClass().getClassLoader().getResource(
								"resoureces/icons/px16/draw-bucket.png")));
				//putValue( Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));
				putValue( Action.SHORT_DESCRIPTION, "Fill an Area");
			}
			public void actionPerformed( ActionEvent e ) {
				toolChangeFillArea();
			}
		};
		aboutAction = new AbstractAction( "About..." ) {
			public void actionPerformed( ActionEvent e){
				new aboutDialog(null, false, "Squares", "In this Application you can draw pictures.\n" +
						"They consist of squares and you can draw by clicking" +
						"one to three times with the left or right mouse Button" +
						"to coloar the sqare\n" +
						"You can also navigate with wasd or arrow keys the yellow marked Field" +
						"and color the currently selected field by pressing enter.");
			}
		};
		
	}
	
	private void initToolBar(){
		JToolBar toolbar = new JToolBar();
		
		toolbar.add( newAction );
		toolbar.add( openAction );
		toolbar.add( saveAction );
		toolbar.addSeparator();
		toolbar.add( undoAction );
		toolbar.add( redoAction );
		toolbar.add( zoomOutAction );
		toolbar.add( zoomInAction );
		toolbar.add( gridAction );
		toolbar.addSeparator();
		toolbar.add( penToolAction );
		toolbar.add( circleToolAction );
		toolbar.add( rectangleToolAction );
		toolbar.add( lineToolAction );
		toolbar.add( fillAreaToolAction );
	    
		this.add( toolbar, BorderLayout.PAGE_START );
	}
	
	private void initMenuBar(){
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu( "File" );
		JMenu editMenu = new JMenu( "Edit" );
		JMenu helpMenu = new JMenu( "Help" );
		//fileMenu
		
		fileMenu.add( newAction );
		fileMenu.addSeparator();
		fileMenu.add( saveAction );
		fileMenu.add( saveAsAction );
		fileMenu.add( openAction );
		fileMenu.addSeparator();
		fileMenu.add( exitAction );
		
		editMenu.add( undoAction );
		editMenu.add( redoAction );
		editMenu.add( zoomInAction );
		editMenu.add( zoomOutAction );
		editMenu.add( gridAction );
		editMenu.addSeparator();
		editMenu.add( penToolAction );
		editMenu.add( circleToolAction );
		editMenu.add( rectangleToolAction );
		editMenu.add( lineToolAction );
		editMenu.add( fillAreaToolAction );
		
		helpMenu.add( aboutAction );
		
		menuBar.add( fileMenu );
		menuBar.add( editMenu );
		menuBar.add( helpMenu );
		this.setJMenuBar( menuBar );
	}
	
	public void save(){
		if(!saved){
			if(path == null)
				saveAs();
			else
				save(new File(path));
			setSave();
		}
	}
	
	public void saveAs(){
		JFileChooser fc = new JFileChooser();
		
		FileNameExtensionFilter gifFilter = new FileNameExtensionFilter( "GIF", "gif" );
		FileNameExtensionFilter pngFilter = new FileNameExtensionFilter(  "PNG", "png" );
		FileNameExtensionFilter bmpFilter = new FileNameExtensionFilter(  "BMP", "bmp" );
		FileNameExtensionFilter lg23sqFilter = new FileNameExtensionFilter( "LG Images", "lg23sq" );
		fc.setFileFilter( gifFilter );
		fc.setFileFilter( pngFilter );
		fc.setFileFilter( bmpFilter );
		fc.setFileFilter( lg23sqFilter );
		
		switch ( fc.showSaveDialog( this ) )
		{
		  case JFileChooser.APPROVE_OPTION:
		    File file = fc.getSelectedFile();

			if( !fc.getFileFilter().accept(file)){
				if(fc.getFileFilter().equals(gifFilter))
					file = new File(file.getAbsolutePath() + ".gif");
				else if(fc.getFileFilter().equals(pngFilter))
					file = new File(file.getAbsolutePath() + ".png");
				else if(fc.getFileFilter().equals(bmpFilter))
					file = new File(file.getAbsolutePath() + ".bmp");
				else if(fc.getFileFilter().equals(lg23sqFilter))
					file = new File(file.getAbsolutePath() + ".lg23sq");
			}
			
		    save(file);
		    
		    path = file.getAbsolutePath();
		    setSave();
		    break;
		  default:
			  return;
		}
		
	}
	
	private void save(File file){
		String[] extentionArray =  file.getName().split(".");
		if(extentionArray.length == 1)
			return;
		if(file.getAbsolutePath().endsWith(".lg23sq"))
			saveLg23sq(file);
		else if(file.getAbsolutePath().endsWith(".gif"))
			saveGif(file);
		else if(file.getAbsolutePath().endsWith(".png"))
			savePng(file);
		else if(file.getAbsolutePath().endsWith(".bmp"))
			saveBmp(file);
		System.out.println(file);
	}
	
	private void saveBmp(File file){
		try {
			Color[][] c = s.exportColors();
			int x = s.exportX();
			int y = s.exportY();
			
			BufferedImage temp = new BufferedImage(x,y, BufferedImage.TYPE_3BYTE_BGR);
			
			for(int i = 0; i < x; i++)
				for(int j = 0; j < y; j++)
					temp.setRGB(i, j, c[i][j].getRGB());
			ImageIO.write(temp, "bmp", file);
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	private void savePng(File file){
		try {
			Color[][] c = s.exportColors();
			int x = s.exportX();
			int y = s.exportY();
			
			BufferedImage temp = new BufferedImage(x,y, BufferedImage.TYPE_3BYTE_BGR);
			
			for(int i = 0; i < x; i++)
				for(int j = 0; j < y; j++)
					temp.setRGB(i, j, c[i][j].getRGB());
			ImageIO.write(temp, "png", file);
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	private void saveGif(File file){
		try {
			Color[][] c = s.exportColors();
			int x = s.exportX();
			int y = s.exportY();
			
			BufferedImage temp = new BufferedImage(x,y, BufferedImage.TYPE_3BYTE_BGR);
			
			for(int i = 0; i < x; i++)
				for(int j = 0; j < y; j++)
					temp.setRGB(i, j, c[i][j].getRGB());
			ImageIO.write(temp, "gif", file);
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	private void saveLg23sq(File file){
		try {
			Color[][] c = s.exportColors();
			int x = s.exportX();
			int y = s.exportY();
			Point p = s.exportSelected();
			
			FileWriter writer = new FileWriter(file);
			StringBuffer temp = new StringBuffer( c.length*c[0].length*4);
			temp.append("lg23sq File, created by SquaresFrame.java by lifesbest23\n");
			temp.append(x + " " + y + "\n");
			temp.append(p.x + " " + p.y + "\n");
			for(Color[] c1:c){
				for(Color c2:c1)
					temp.append(c2.getRGB() + " ");
				temp.append("\n");
			}
			writer.write(temp.toString());
			writer.close();
			setSave();
		} catch (IOException e) { e.printStackTrace(); }
		
	}
	
	public void open(){
		if(!saved){
			int option = JOptionPane.showConfirmDialog(this, "Save Changes?", "Save",
					JOptionPane.YES_NO_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION)
				save();
			else if(option == JOptionPane.CANCEL_OPTION)
				return;
		}
		JFileChooser fc = new JFileChooser();
		
		FileNameExtensionFilter gifFilter = new FileNameExtensionFilter( "GIF", "gif" );
		FileNameExtensionFilter pngFilter = new FileNameExtensionFilter(  "PNG", "png" );
		FileNameExtensionFilter bmpFilter = new FileNameExtensionFilter(  "BMP", "bmp" );
		FileNameExtensionFilter lg23sqFilter = new FileNameExtensionFilter( "LG Images", "lg23sq" );
		fc.setFileFilter( gifFilter );
		fc.setFileFilter( pngFilter );
		fc.setFileFilter( bmpFilter );
		fc.setFileFilter( lg23sqFilter );
		
		switch ( fc.showOpenDialog( this ) )
		{
		  case JFileChooser.APPROVE_OPTION:
		    File file = fc.getSelectedFile();
		    open(file);
		    s.revalidate();
		    break;
		  default:
			  return;
		}
		
	}
	
	private void open(File file){
	    path = file.getAbsolutePath();
	    setSave();
	    
		if(file.getAbsolutePath().endsWith(".lg23sq"))
			openLg23sq(file);
		else if(file.getAbsolutePath().endsWith(".gif"))
			openGif(file);
		else if(file.getAbsolutePath().endsWith(".png"))
			openPng(file);
		else if(file.getAbsolutePath().endsWith(".bmp"))
			openBmp(file);
	}
	
	private void openPng(File file){
		openGif(file);
	}
	
	private void openBmp(File file){
		openGif(file);
	}
	
	private void openGif(File file){
		try {
			BufferedImage img = ImageIO.read(file);
			if(img.getHeight() > 256 || img.getWidth() > 256){
				JOptionPane.showMessageDialog(this, "The Image is too Big!");
				return;
			}
			Color[][] c = new Color[img.getWidth()][img.getHeight()];
			for(int x = 0; x < img.getWidth(); x++){
				for(int y = 0; y < img.getHeight(); y++){
					c[x][y] = new Color(img.getRGB(x, y));
				}
			}
			s.load(c, new Point(0,0), img.getHeight(), img.getWidth());
			s.revalidate();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	private void openLg23sq(File file){
		int x, y, px, py;
		Color[][] c;
		try {
			Scanner sc = new Scanner(file);
			if( sc.nextLine().equals("lg23sq File, created by SquaresFrame.java by lifesbest23")){
				x = sc.nextInt();
				y = sc.nextInt();
				px = sc.nextInt();
				py = sc.nextInt();
				c = new Color[x][y];
				for(int i = 0; i < x; i++)
					for(int j = 0; j < y; j++)
						c[i][j] = new Color(sc.nextInt());
				s.load(c, new Point(px,py), x, y);
				s.revalidate();
			}
			sc.close();
		} catch (Exception e) { e.printStackTrace(); }
	}
	
	public void newFile(){
		if(!saved){
			int option = JOptionPane.showConfirmDialog(this, "Save Changes?", "Save",
					JOptionPane.YES_NO_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION)
				save();
			else if(option == JOptionPane.CANCEL_OPTION)
				return;
		}
		NewDialog nd = new NewDialog(this, true);
		if(nd.isCanceled())
			return;
		s.plane(nd.x, nd.y);
		this.setTitle(String.format("Squares Editor - *New File - %d_%d*", nd.x, nd.y));
		s.revalidate();
	}
	
	public void setSave(){
		if( saved == false)
			this.setTitle("Squares Editor - " + this.path);
		saved = true;
	}
	
	public void clearSaved(){
		if(saved == true)
			this.setTitle("Squares Editor - " + "*" + this.path + "*");
		saved = false;
	}
	
	private void undo(){
		s.undo();
	}
	
	private void redo(){
		s.redo();
	}
	
	private void zoomIn(){
		s.zoomIn();
		s.revalidate();
	}
	
	private void zoomOut(){
		s.zoomOut();
		s.revalidate();
	}
	
	private void toggleGrid(){
		s.setGrid(!s.getGrid());
		s.revalidate();
	}
	
	private void toolChangeFillArea(){
		s.setFillAreaTool();
	}
	
	private void toolChangeLine(){
		s.setLineTool();
	}
	
	private void toolChangeRectangle(){
		s.setRectangleTool();
	}
	
	private void toolChangeCircle(){
		s.setCircleTool();
	}
	
	private void toolChangePen(){
		s.setFreeDrawTool();
	}
	
	public void exit(){
		if(!saved){
			int option = JOptionPane.showConfirmDialog(this, "Save Changes bevore exit?", "Save",
					JOptionPane.YES_NO_CANCEL_OPTION);
			if(option == JOptionPane.OK_OPTION)
				save();
			else if(option == JOptionPane.NO_OPTION)
				System.exit(0);
		}
		else
			System.exit(0);
		
	}
		
	@Override public void keyPressed(KeyEvent e) {
		int kc = e.getKeyCode();
		char k = Character.toLowerCase(e.getKeyChar());
		
		if( k == 'w' || kc == KeyEvent.VK_UP)
			s.selectionUp();
		else if( k == 's' || kc == KeyEvent.VK_DOWN)
			s.selectionDown();
		else if( k == 'd' || kc == KeyEvent.VK_RIGHT)
			s.selectionRight();
		else if( k == 'a' || kc == KeyEvent.VK_LEFT)
			s.selectionLeft();
		else if( kc == KeyEvent.VK_ENTER)
			s.selectionSet();
	}
	
	@Override public void keyReleased(KeyEvent e) {}

	@Override public void keyTyped(KeyEvent e) {}
	
	public static void main(String[] args) {
		if(args.length > 0){
			for(int i = 0; i < args.length; i++){
				if(new File(args[i]).exists()){
					new SquaresFrame(args[i]);
				}	
			}
		}
		else{
			new SquaresFrame("");
		}
	}
}
