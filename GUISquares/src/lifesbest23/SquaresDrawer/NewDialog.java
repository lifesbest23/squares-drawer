package lifesbest23.SquaresDrawer;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import lifesbest23.common.swing.WindowAdapter;

 
public class NewDialog extends JDialog {
	private static final long serialVersionUID = 7994591613105226823L;
	
	private SpinnerNumberModel heightModel, widthModel;
	private JSpinner height, width;
	public int x;

	public int y;
	private boolean canceled = true;
	
	public NewDialog(JFrame frame, boolean modal){
		//WindowPreparation
		super(frame, modal);
		this.setLayout(new GridLayout(3, 2, 5, 5));
		this.setTitle("New");
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter(){
			@Override public void windowClosing( WindowEvent event )
	    	{
				close();
			}
		});
				
		//Spinner
		heightModel = new SpinnerNumberModel( 10.0, 1.0, 255.0, 1.0 );
		widthModel = new SpinnerNumberModel( 10.0, 1.0, 255.0, 1.0 );
		height = new JSpinner(heightModel);
		width = new JSpinner(widthModel);
		
		JButton newButton = new JButton("New");
		newButton.addActionListener(new ActionListener(){
			@Override public void actionPerformed(ActionEvent arg0) {
				newButtonAction();
			}
		});
		
		JButton cancelButton = new JButton ("Cancel");
		cancelButton.addActionListener(new ActionListener(){
			@Override public void actionPerformed(ActionEvent arg0) {
				cancelButtonAction();
			}
		});
		
		this.add(new JLabel("Width  :"));
		this.add(width);
		this.add(new JLabel("Height :"));
		this.add(height);
		this.add(cancelButton);
		this.add(newButton);
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	private void newButtonAction(){
		y = heightModel.getNumber().intValue();
		x = widthModel.getNumber().intValue();
		canceled = false;
		close();
	}
	
	private void cancelButtonAction(){
		close();
	}
	
	private void close(){
		this.setVisible(false);
		this.dispose();
	}
	
	public boolean isCanceled(){
		return canceled;
	}
}
